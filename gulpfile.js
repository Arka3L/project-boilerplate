var gulp = require('gulp');
var config = require('./config.json');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var eslint = require('gulp-eslint');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var babelify = require('babelify');
var browserify = require('browserify');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var cssnano = require('gulp-cssnano');


var SRC_SCRIPTS = config.source.scripts;
var SRC_STYLES = config.source.styles;
var BUILD_SCRIPTS = config.build.scripts;
var BUILD_STYLES = config.build.styles;

function gobbleError(error) {
	console.error(error.toString());
	this.emit('end');
}

gulp.task('jslint', function() {
	return gulp.src(SRC_SCRIPTS.files)
        .pipe(eslint())
        .pipe(eslint.format())
        // To have the process exit with an error code (1) on 
        // lint error, return the stream and pipe to failAfterError last. 
        .pipe(eslint.failAfterError());
});

gulp.task('scripts', ['jslint'], function() {
	return browserify({ entries: SRC_SCRIPTS.srcFile })
		.transform(babelify, { global: true })
		.bundle()
		.on('error', gobbleError)
		.pipe(source(SRC_SCRIPTS.srcFile))
		.pipe(buffer())
		.pipe(rename(BUILD_SCRIPTS.outputName + '.js'))
		.pipe(gulp.dest(BUILD_SCRIPTS.path))
		.pipe(uglify({ mangle: true }))
		.on('error', gobbleError)
		.pipe(rename(BUILD_SCRIPTS.outputName + '.min.js'))
		.pipe(gulp.dest(BUILD_SCRIPTS.path));
});

// Outputs two files. One for local development and another for release
gulp.task('styles', function() {
	return gulp.src(SRC_STYLES.srcFile)
		.pipe(sass())
		.on('error', gobbleError)
		.pipe(autoprefixer(SRC_STYLES.supported))
		.pipe(cssnano())
		.on('error', gobbleError)
		.pipe(rename(BUILD_STYLES.outputName))
		.pipe(gulp.dest(BUILD_STYLES.path));
});

gulp.task('views', function() {
	return gulp.src('./src/**/*.html')
		.pipe(gulp.dest('./build'));
})


gulp.task('default', ['scripts', 'styles', 'views'], function() {
	gulp.watch(SRC_SCRIPTS.files, ['scripts']);
	gulp.watch(SRC_STYLES.files, ['styles']);
	gulp.watch('./src/**/*.html', ['views']);
});